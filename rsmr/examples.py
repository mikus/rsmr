# -*- coding: utf-8 -*-
# Copyright 2014 Mikołaj Olszewski
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from rsmr import MapReduce


# Words counter

import multiprocessing
import string


def wc_map(filename):
    """
    Args:
        filename (str): name of file to process
    """
    TR = string.maketrans(string.punctuation, ' ' * len(string.punctuation))

    print multiprocessing.current_process().name, 'reads', filename

    with open(filename, 'rt') as f:
        for line in f:
            if line.lstrip().startswith('#'):  # omit comments
                continue
            line = line.translate(TR)  # omit punctuation
            for word in line.split():
                word = word.lower()
                if word.isalpha():
                    yield word, 1


def wc_reduce(word, occurrences):
    """
    Args:
        word (str): current word
        occurrences (list): list of ones (for each word occurrence)
    """
    yield word, sum(occurrences)


def wordscounter():
    import operator
    import glob

    input_files = glob.glob('*.py')

    mapper = MapReduce(wc_map, wc_reduce)
    word_counts = mapper(input_files)
    word_counts.sort(key=operator.itemgetter(1), reverse=True)

    print '\nTOP 20 WORDS\n'
    top20 = word_counts[:20]
    longest = max(len(word) for word, count in top20)
    for word, count in top20:
        print '%-*s: %5s' % (longest+1, word, count)


if __name__ == '__main__':
    wordscounter()