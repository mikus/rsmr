#!/usr/bin/env python

# -*- coding: utf-8 -*-

from distutils.core import setup

setup(name='RSMR',
      version='1.0',
      description='Really Simple Map Reduce',
      author=u'Mikołaj Olszewski',
      author_email='mikus156@gmail.com',
      url='https://bitbucket.org/mikus/rsmr',
      packages=['rsmr'],
      license='Apache v2.0',
     )
