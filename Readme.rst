=========================
Really Simple Map Reduce
=========================

A simple multiprocess helper for better understanding **Map-Reduce** programming model.

License
=======
`Apache License v. 2.0 <http://www.apache.org/licenses/LICENSE-2.0>`__